#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
#include "Helper.h"

using namespace std;

#define PATH_MAX 2048
#define ERROR_INFO "\nError : " << GetLastError() << endl
#define COMMAND messageVector[0].c_str()
#define PARAM1 messageVector[1].c_str()
#define FILE_TYPE messageVector[0].substr(messageVector[0].find(".")).c_str()
#define FILE_NAME messageVector[0].substr(0, messageVector[0].find(".")).c_str()

void pwd();
void cd(string newPath);
void create(string fileName);
void ls(string dir);
void secret();
void execute(string fileName);


int main()
{
	Helper data;
	string message;
	vector<string> messageVector;

	while (message != "quit" && message != "exit")
	{
		cout << ">>";
		getline(cin, message);

		data.trim(message);
		messageVector = data.get_words(message);

		if (!strcmp(COMMAND, "pwd"))
		{
			pwd();
		}
		else if (!strcmp(COMMAND, "cd"))
		{
			if (messageVector.size() > 1)
				cd(PARAM1);
			else
				cout << "Not enough parameters\n";
		}
		else if (!strcmp(COMMAND, "create"))
		{
			if (messageVector.size() > 1)
				create(PARAM1);
			else
				cout << "Not enough parameters\n";
		}
		else if (!strcmp(COMMAND, "ls"))	
		{
			if (messageVector.size() > 1)
				ls(PARAM1);
			else
				ls("");
		}
		else if (!strcmp(COMMAND, "secret"))
		{
			secret();
		}
		else if (!strcmp(FILE_TYPE, ".exe"))
		{
			execute(FILE_NAME);
		}
	}
}

void pwd()
{
	char szCurrentDirectory[PATH_MAX + 1];
	if (!GetCurrentDirectory(PATH_MAX, szCurrentDirectory))
	{
		cout << "Failed to get dir" << ERROR_INFO;
		return;
	}
	szCurrentDirectory[PATH_MAX] = '\0';

	cout << szCurrentDirectory << endl;
}

void cd(string newPath)
{
	if (!SetCurrentDirectory(newPath.c_str()))
		cout << "Failed to change dir" << ERROR_INFO;
}

void create(string fileName)
{
	if (CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL) == INVALID_HANDLE_VALUE)
		cout << "Failed to create file" << ERROR_INFO;
}

void ls(string dir)
{
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	HANDLE hFind = INVALID_HANDLE_VALUE;

	if (!strcmp(dir.c_str(), ""))
	{
		char buffer[PATH_MAX + 1];

		if (!GetCurrentDirectory(PATH_MAX, buffer))
		{
			cout << "Failed to get dir" << ERROR_INFO;
			return;
		}

		buffer[PATH_MAX] = '\0';
		dir = buffer;
	}

	if (dir.length() > (MAX_PATH - 3)){
		cout << "Failed to list files\nPath too long";
		return;
	}

	cout << "\nTarget directory is \n\n" << dir;

	strcpy_s(szDir, dir.c_str());
	strncat_s(szDir, "\\*", MAX_PATH);

	hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind){
		cout << "Failed to list files" << ERROR_INFO;
		return;
	}

	while (FindNextFile(hFind, &ffd) != 0)
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			cout << ffd.cFileName << "<DIR>\n";
		}
		else
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			cout << ffd.cFileName << " " << filesize.QuadPart << " bytes\n";
		}
	}
}

void secret()
{
	HINSTANCE hinstLib;
	PROC ProcAdd;
	BOOL fFreeResult, fRunTimeLinkSuccess = FALSE;

	// Get a handle to the DLL module.

	hinstLib = LoadLibrary(TEXT("Secret.dll"));

	// If the handle is valid, try to get the function address.

	if (hinstLib != NULL)
	{
		ProcAdd = (PROC)GetProcAddress(hinstLib, "TheAnswerToLifeTheUniverseAndEverything");

		// If the function address is valid, call the function.

		if (ProcAdd)
		{
			fRunTimeLinkSuccess = TRUE;
			cout << (ProcAdd)();
		}
		// Free the DLL module.

		fFreeResult = FreeLibrary(hinstLib);
	}

	// If unable to call the DLL function, use an alternative.
	if (!fRunTimeLinkSuccess)
		printf("Message printed from executable\n");
}

void execute(string fileName)
{
	STARTUPINFO info = { sizeof(info) };
	PROCESS_INFORMATION procInfo;
	LPDWORD code = new DWORD();

	if (CreateProcessA(fileName.c_str(), NULL, NULL, NULL, TRUE, 0, NULL, NULL, &info, &procInfo))
	{
		int flag = WaitForSingleObject(procInfo.hProcess, INFINITE);

		if (flag != WAIT_ABANDONED && flag != WAIT_OBJECT_0 && flag != WAIT_TIMEOUT && flag != WAIT_FAILED)
		{
			cout << "Failed waiting for the object" << ERROR_INFO;
		}
		if (!GetExitCodeProcess(procInfo.hProcess, code))
		{
			cout << "Unable to run process" << ERROR_INFO;
		}
		if (!CloseHandle(procInfo.hProcess))
		{
			cout << "Failed closing the process" << ERROR_INFO;
		}
		if (!CloseHandle(procInfo.hThread))
		{
			cout << "Failed closing the thread" << ERROR_INFO;
		}

		cout << "Existed with " << *code << endl;
	}
	else
	{
		cout << "Failed to open proccess" << ERROR_INFO;
	}
}
